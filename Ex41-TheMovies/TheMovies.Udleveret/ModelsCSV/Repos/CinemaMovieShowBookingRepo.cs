﻿using System;
using System.Collections.Generic;
using System.Linq;
using TheMovies.Models;
using System.IO;

namespace TheMovies.Repos
{
    public class CinemaMovieShowBookingRepo : IRepository<CinemaMovieShowBooking>
    {
        private List<CinemaMovieShowBooking> entries;

        public CinemaMovieShowBookingRepo()
        {
            entries = new List<CinemaMovieShowBooking>();
            LoadRepo();
        }

        public IEnumerable<CinemaMovieShowBooking> GetAll()
        {
            // Return a copy of the internal datastructure

            return entries.ToList();
        }

        public CinemaMovieShowBooking GetById(object id)
        {
            throw new NotImplementedException();
        }

        public void Add(CinemaMovieShowBooking obj)
        {

            bool duplikat = false;
            foreach(var e in entries)
            {   //check if the new obj we wanna add already exists in entries. If it does, then it is a duplicat
                if(e.CinemaName == obj.CinemaName && e.CinemaTown == obj.CinemaTown)
                {
                    duplikat = true;
                    break;
                }
            }
            //if a duplicat is found then throw exception
            if (duplikat) throw new Exception("duplikat");
            //if duplicat is not found then add
            entries.Add(obj);
      

            //throw new NotImplementedException();
        }

        public void Delete(CinemaMovieShowBooking obj)
        {
            if(obj.CinemaName != null)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i].CinemaName == obj.CinemaName && entries[i].CinemaTown == obj.CinemaTown)
                    {
                        entries.Remove(entries[i]);
                        i = -1;
                    }
                }
            }
            
        }

        public void Update(CinemaMovieShowBooking obj, CinemaMovieShowBooking newValues)
        {

            foreach(var a in entries)
            {
                if (a.CinemaTown == obj.CinemaTown && a.CinemaName == obj.CinemaName)
                {
                    a.CinemaName = newValues.CinemaName;
                    a.CinemaTown = newValues.CinemaTown;
                }
            }

            //throw new NotImplementedException();
        }

        private void LoadRepo()
        {

            using (StreamReader reader = new StreamReader(@"TheMovies.CSV"))
            {
                //læser den første linje som er header men gør ikke noget med den
                string firstLine = reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] values = line.Split(';');

                    CinemaMovieShowBooking blah = new CinemaMovieShowBooking();
                    blah.CinemaName = values[0];
                    blah.CinemaTown = values[1];
                    blah.ShowDateTime = DateTime.Parse(values[2]);
                    blah.MovieTitle = values[3];
                    blah.MovieGenre = values[4];
                    blah.MovieDuration = HoursToMinutes(values[5]);
                    blah.MovieDirector = values[6];
                    blah.MovieReleaseDate = DateTime.Parse(values[7]);
                    blah.BookingMail = values[8];
                    blah.BookingPhone = values[9];

                    entries.Add(blah);
                }
            }
        }

        private int HoursToMinutes(string input)
        {
            string[] values = input.Split(':');
            Int32.TryParse(values[0], out int var1);
            Int32.TryParse(values[1], out int var2);
            return var1 * 60 + var2;
        }

        private void SaveRepo()
        {
            // Implement this method !
        }
    }
}
