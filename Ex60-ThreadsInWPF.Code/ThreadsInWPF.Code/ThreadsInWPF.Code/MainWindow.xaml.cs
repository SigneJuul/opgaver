﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ThreadsInWPF.Code
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnPutIn1_Click(object sender, RoutedEventArgs e)
        {
            if (lbFruits.SelectedItem != null)
            {
                var fruit = (lbFruits.SelectedItem as ListBoxItem).Content;
                lbBlender1.Items.Add(new ListBoxItem { Content = fruit });
            }
            btnClean1.IsEnabled = true;
        }

        private void BtnPutIn2_Click(object sender, RoutedEventArgs e)
        {
            if (lbFruits.SelectedItem != null)
            {
                var fruit = (lbFruits.SelectedItem as ListBoxItem).Content;
                lbBlender2.Items.Add(new ListBoxItem { Content = fruit });
            }
            btnClean2.IsEnabled = true;
        }

        private void BtnBlend1_Click(object sender, RoutedEventArgs e)
        {

            Thread a = new Thread(Blend1);
            a.Start();
            //Blend1();
        }

        private void BtnBlend2_Click(object sender, RoutedEventArgs e)
        {
            Thread b = new Thread(Blend2);
            b.Start();
            //Blend2();
        }

      
        private void Blend1()
        {
            
            btnBlend1.Dispatcher.Invoke(new Action(() => btnBlend1.IsEnabled = false), System.Windows.Threading.DispatcherPriority.Normal, null);
            btnBlend1.Dispatcher.Invoke(new Action(() => btnClean1.IsEnabled = false), DispatcherPriority.Normal, null);
            
            int blendTime = 10;
            for (int i = 0; i < blendTime; i++)
            {
                tBlock1.Dispatcher.Invoke(new Action( ()  => tBlock1.Text = $"Blending {i+1}"), DispatcherPriority.Normal, null);
                bar1.Dispatcher.Invoke(new Action(() => bar1.Value = i+1), DispatcherPriority.Normal, null);
                Thread.Sleep(1000);
            }
            
            lbBlender1.Dispatcher.Invoke(new Action( () => tBlock1.Text = "Juice Ready"), DispatcherPriority.Normal, null);
            btnBlend1.Dispatcher.Invoke(new Action(() => btnBlend1.IsEnabled = true), DispatcherPriority.Normal, null);
            btnBlend1.Dispatcher.Invoke(new Action(() => btnClean1.IsEnabled = true), DispatcherPriority.Normal, null);
        }

        private void Blend2()
        {
            btnBlend2.Dispatcher.Invoke(new Action(() => btnBlend2.IsEnabled = false)); // behøver ikke DispatcherPriority men en mulighed
            btnBlend2.Dispatcher.Invoke(new Action(() => btnClean2.IsEnabled = false));
            int blendTime = 10;
            for (int i = 0; i < blendTime; i++)
            {
                tBlock2.Dispatcher.Invoke(new Action( () => tBlock2.Text = $"Blending {i}"), DispatcherPriority.Normal, null);
                bar2.Dispatcher.Invoke(new Action(() => bar2.Value = i+1), DispatcherPriority.Normal, null);
                Thread.Sleep(1000);
            }
            
            tBlock2.Dispatcher.Invoke(new Action( () => tBlock2.Text = "Juice Ready"), DispatcherPriority.Normal, null);
            btnBlend2.Dispatcher.Invoke(new Action(() => btnBlend2.IsEnabled = true), DispatcherPriority.Normal, null);
            btnBlend2.Dispatcher.Invoke(new Action(() => btnClean2.IsEnabled = true), DispatcherPriority.Normal, null);
        }

        private void btnClean1_Click(object sender, RoutedEventArgs e)
        {
            Thread c = new Thread(Clean1);
            c.Start();
        }

        private void btnClean2_Click(object sender, RoutedEventArgs e)
        {
            Thread d = new Thread(Clean2);
            d.Start();
        }

        private void Clean1()
        {
            btnClean1.Dispatcher.Invoke(new Action(() => lbBlender1.Items.Clear()), DispatcherPriority.Normal, null);
            tBlock1.Dispatcher.Invoke(new Action(() => tBlock1.Text = "Cleaned"), DispatcherPriority.Normal, null);
            btnClean1.Dispatcher.Invoke(new Action(() => btnClean1.IsEnabled = false), DispatcherPriority.Normal, null);
            btnClean1.Dispatcher.Invoke(new Action(() => bar1.Value = 0), DispatcherPriority.Normal, null);
        }

        private void Clean2()
        {
            btnClean2.Dispatcher.Invoke(new Action(() => lbBlender2.Items.Clear()), DispatcherPriority.Normal, null);
            tBlock2.Dispatcher.Invoke(new Action(() => tBlock2.Text = "Cleaned"), DispatcherPriority.Normal, null);
            btnClean2.Dispatcher.Invoke(new Action(() => btnClean2.IsEnabled = false), DispatcherPriority.Normal, null);
            bar2.Dispatcher.Invoke(new Action(() => bar2.Value = 0), DispatcherPriority.Normal, null);
        }


    }
}
