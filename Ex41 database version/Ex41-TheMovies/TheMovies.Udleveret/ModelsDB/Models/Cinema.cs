﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelsDB.Models
{
    public class Cinema
    {
        public int CinemaID { get; set; }
        public string CinemaName { get; set; }
        public string CinemaTown { get; set; }
    }
}
