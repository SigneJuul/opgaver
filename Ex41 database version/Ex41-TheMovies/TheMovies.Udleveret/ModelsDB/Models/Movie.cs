﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelsDB.Models
{
    class Movie
    {
        public int MovieID { get; set; }
        public string MovieTitle { get; set; }
        public int MovieDuration { get; set; }  // The duration of the movie in minutes
        public string MovieDirector { get; set; }
        public DateTime MovieReleaseDate { get; set; }

    }
}
