﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelsDB.Models
{
    class Booking
    {
        public string BookingMail { get; set; }
        public string BookingPhone { get; set; }
        public int ShowID { get; set; }
    }
}

