﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelsDB.Models
{
    class MovieGenre
    {
        public int MovieID { get; set; }
        public int GenreID { get; set; }
    }

}
