﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelsDB.Models
{
    class Show
    {
        public int ShowID { get; set; }
        public DateTime ShowDateTime { get; set; }
        public int MovieID { get; set; }
        public int CinemaID { get; set; }

    }
}
