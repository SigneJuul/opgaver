﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TheMovies.Models;

namespace TheMovies.Repos
{
    public class CinemaMovieShowBookingRepo : IRepository<CinemaMovieShowBooking>
    {
        private List<CinemaMovieShowBooking> entries;

        private string connectionString = "Server=10.56.8.35;Database=B_DB49_2020;User Id=B_STUDENT49;Password=B_OPENDB49";

        public CinemaMovieShowBookingRepo()
        {
            entries = new List<CinemaMovieShowBooking>();
            LoadRepo();
        }

        public void Add(CinemaMovieShowBooking obj)
        {
            bool duplikat = false;
            foreach (var e in entries)
            {   //check if the new obj we wanna add already exists in entries. If it does, then it is a duplicat
                if (e.CinemaName == obj.CinemaName && e.CinemaTown == obj.CinemaTown)
                {
                    duplikat = true;
                    break;
                }
            }
            //if a duplicat is found then throw exception
            if (duplikat) throw new Exception("duplikat");
            //if duplicat is not found then add
            entries.Add(obj);



        }

        public void Delete(CinemaMovieShowBooking obj)
        {
            if (obj.CinemaName != null)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i].CinemaName == obj.CinemaName && entries[i].CinemaTown == obj.CinemaTown)
                    {
                        entries.Remove(entries[i]);
                        i = -1;
                    }
                }
            }
        }

        public IEnumerable<CinemaMovieShowBooking> GetAll()
        {
            //throw new NotImplementedException();

            return entries.ToList(); 
        }

        public CinemaMovieShowBooking GetById(object id)
        {
            throw new NotImplementedException();
        }

        public void Update(CinemaMovieShowBooking obj, CinemaMovieShowBooking newValues)
        {
            throw new NotImplementedException();
        }

        // Implement the interface methods shown in the DCD!

        private void LoadRepo()
        {   //create a connection to the databse
            using (SqlConnection connection = new SqlConnection(
               connectionString))
            {
                //open the connection
                connection.Open();

                //Create the query
                string query = "SELECT * FROM dbo.CinemaMovieShowBooking";

                //Create SQL command
                SqlCommand command = new SqlCommand(query, connection);

                //Read 
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    // create new object
                    var entry = new CinemaMovieShowBooking();

                    //convert database data through casting (string) and assign to object
                    entry.CinemaName = (string)reader[1];
                    entry.CinemaTown = (string)reader[2];
                    entry.MovieTitle = (string)reader[3];
                    entry.MovieGenre = (string)reader[4];
                    entry.MovieDuration = (int)reader[5];
                    entry.MovieDirector = (string)reader[6];
                    entry.MovieReleaseDate = (DateTime)reader[7];
                    entry.ShowDateTime = (DateTime)reader[8];
                    entry.BookingMail = (string)reader[9];
                    entry.BookingPhone = (string)reader[10];

                    entries.Add(entry);

                }
            }
        }
        private void SaveRepo()
        {
            //using (SqlConnection connection = new SqlConnection(connectionString)) 
            //{
            //    connection.Open();

            //    string commandText = "UPDATE dbo.CinemaMovieShowBooking SET "

            //}
        }
    }
}
