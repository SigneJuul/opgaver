﻿using ModelsDB.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TheMovies.Models;
using TheMovies.Repos;

namespace ModelsDB.Repos
{
    public class CinemaRepo : IRepository<Cinema>
    {
        private string connectionString = "Server=10.56.8.35;Database=B_DB49_2020;User Id=B_STUDENT49;Password=B_OPENDB49";

        private List<Cinema> entries; 
        public void Add(Cinema obj)
        {
            entries.Add(obj);
        }

        public void Delete(Cinema obj)
        {
            entries.Remove(obj);
        }

        public IEnumerable<Cinema> GetAll()
        {
            return entries.ToList();
        }

        public Cinema GetById(object id)
        {
            return entries.Find();
        }

        public void Update(Cinema obj, Cinema newValues)
        {
            throw new NotImplementedException();
        }
    }
}
