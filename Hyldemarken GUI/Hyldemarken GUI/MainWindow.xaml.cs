﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hyldemarken_GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void planting_btn_Click(object sender, RoutedEventArgs e)
        {
            
            ChoosePotato_window potato_Window = new ChoosePotato_window();
            potato_Window.Show();
            this.Close();
             
            
        }

        private void beginHarvet_btn_Click(object sender, RoutedEventArgs e)
        {
            ChoosePotatoStartHarvest cpsh = new ChoosePotatoStartHarvest();
            cpsh.Show();
            this.Close();
  
        }

        private void harvest_btn_Click(object sender, RoutedEventArgs e)
        {
            ChoosePotatoHarvest cph = new ChoosePotatoHarvest();
            cph.Show();
            this.Close();
        }
    }
}
