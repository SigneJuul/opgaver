﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hyldemarken_GUI
{
    /// <summary>
    /// Interaction logic for PlantingInformation.xaml
    /// </summary>
    public partial class PlantingInformation : Window 
    {

        public PlantingInformation()
        {
            InitializeComponent();
            Potato_label.Content = ChoosePotato_window.chosenPotato;
            Gem_btn.IsEnabled = false;
            
        }

        bool amount = false;
        bool area = false;

        private void add_btn_Click_1(object sender, RoutedEventArgs e)
        {
            AddFertilizer af = new AddFertilizer();
            af.ShowDialog();

            string result = af.fertilizerName;
            fertilizer_tbx.Text = result;
        }

        private void Annuller_btn_Click(object sender, RoutedEventArgs e)
        {
            ChoosePotato_window chw = new ChoosePotato_window();
            chw.Show();
            this.Close();
        }


        private void amount_tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double fAmount = Convert.ToDouble(amount_tbx.Text);
                amount = true;
                SaveButtonEnabled();
            }
            catch
            {
                MessageBox.Show("Input skal være tal", "Forkert input", MessageBoxButton.OK, MessageBoxImage.Information);
                
            }

            
        }
        private void area_tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
                try
                {
                    double fArea = Convert.ToDouble(area_tbx.Text);
                    area = true;
                    SaveButtonEnabled();
                }
                catch
                {
                    MessageBox.Show("Input skal være tal", "Forkert input", MessageBoxButton.OK, MessageBoxImage.Information);

                };

        }


        private void SaveButtonEnabled()
        {
            if(amount && area)
            {
                Gem_btn.IsEnabled = true; 
            }
        }

        
    }
}
