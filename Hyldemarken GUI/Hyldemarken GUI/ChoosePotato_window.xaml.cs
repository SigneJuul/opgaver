﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hyldemarken_GUI
{
    /// <summary>
    /// Interaction logic for ChoosePotato_window.xaml
    /// </summary>
    public partial class ChoosePotato_window : Window
    {
        public ChoosePotato_window()
        {
            InitializeComponent();
            fill_cmb();
            Accept_btn.IsEnabled = false;
        }

        public static string chosenPotato;


        private ObservableCollection<string> potatoes = new ObservableCollection<string>
        {
            "potato1",
            "potato2",
            "potato3"
        };

        

        public ObservableCollection<string> Potato
        {
            get { return potatoes; }
            set { potatoes = value; }
        }

        void fill_cmb()
        {
            foreach(var p in Potato)
            {
                potato_cmb.Items.Add(p);
            }
        }


        private void Annuller_btn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Close();
        }

        private void Accept_btn_Click(object sender, RoutedEventArgs e)
        {
            chosenPotato = (string)potato_cmb.SelectedValue;
            

            PlantingInformation plantingInformation = new PlantingInformation();
            plantingInformation.Show();
            this.Close();
            

        }

        private void potato_cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Accept_btn.IsEnabled = true;
        }
    }
}
