﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hyldemarken_GUI
{
    /// <summary>
    /// Interaction logic for HarvestInformation.xaml
    /// </summary>
    public partial class HarvestInformation : Window
    {
        public HarvestInformation()
        {
            InitializeComponent();
            Potato_label.Content = ChoosePotatoHarvest.chosenPotato;
            save_btn.IsEnabled = false;
        }

        private void cancel_btn_Click(object sender, RoutedEventArgs e)
        {
            ChoosePotatoHarvest cph = new ChoosePotatoHarvest();
            cph.Show();
            this.Close();

        }

        private void amount_tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double harvestAmount = Convert.ToDouble(amount_tbx.Text);
                save_btn.IsEnabled = true;
            }
            catch
            {
                MessageBox.Show("Input skal være tal", "Forkert input", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
