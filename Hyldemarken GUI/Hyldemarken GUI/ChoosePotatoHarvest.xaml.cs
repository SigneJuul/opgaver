﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hyldemarken_GUI
{
    /// <summary>
    /// Interaction logic for ChoosePotatoHarvest.xaml
    /// </summary>
    public partial class ChoosePotatoHarvest : Window
    {
        public ChoosePotatoHarvest()
        {
            InitializeComponent();
            Accept_btn.IsEnabled = false;
            fill_cmb();
        }

        public static string chosenPotato;


        private ObservableCollection<string> potatoes = new ObservableCollection<string>
        {
            "potato1",
            "potato2",
            "potato3"
        };



        public ObservableCollection<string> Potato
        {
            get { return potatoes; }
            set { potatoes = value; }
        }

        void fill_cmb()
        {
            foreach (var p in Potato)
            {
                potato_cmb.Items.Add(p);
            }
        }

        private void Annuller_btn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Close();
        }

        private void Accept_btn_Click(object sender, RoutedEventArgs e)
        {
            chosenPotato = (string)potato_cmb.SelectedValue;

            HarvestInformation hi = new HarvestInformation();
            hi.Show();
            this.Close();
        }

        private void potato_cmb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Accept_btn.IsEnabled = true;
        }
    }
}
