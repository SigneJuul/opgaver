﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hyldemarken_GUI
{
    /// <summary>
    /// Interaction logic for AddFertilizer.xaml
    /// </summary>
    public partial class AddFertilizer : Window
    {
        public AddFertilizer()
        {
            InitializeComponent();
            save_btn.IsEnabled = false;
        }
        public string fertilizerName;
        public double fertilizerAmount;

        bool name = false;
        bool amount = false;
        private void annuller_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void save_btn_Click(object sender, RoutedEventArgs e)
        {
            fertilizerName = fertilizerName_txb.Text;
            fertilizerAmount = Convert.ToDouble(fertilizerAmount_txb.Text);
            
            this.Close();
            
        }

        private void fertilizerName_txb_TextChanged(object sender, TextChangedEventArgs e)
        {
            name = true;
            SaveButtonEnabled();
        }

        private void fertilizerAmount_txb_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double fAmount = Convert.ToDouble(fertilizerAmount_txb.Text);
                amount = true;
                SaveButtonEnabled();
            }
            catch
            {
                MessageBox.Show("Input skal være tal", "Forkert input", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void SaveButtonEnabled()
        {
            if(name && amount)
            {
                save_btn.IsEnabled = true;
            }
        }


    }
}
