﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityLib1;

namespace Morgengry
{
    public class CourseRepository      // see notes in "BookRepository"
    {
        private List<Course> courses = new List<Course>();

        public void AddCourse(Course course)
        {
            courses.Add(course);
        }

        public Course GetCourse(string name)
        {
            foreach(Course i in courses)
            {
                if(i.Name == name)
                {
                    return i;
                }
            }
            return null;
        }

        public double GetTotalValue()
        {
            double fullPrice = 0;
            foreach(Course i in courses)
            {
                fullPrice += Utility.GetValueOfCourse(i);  //gets the value from the utility class
            }
            return fullPrice;
        }

    }
}
