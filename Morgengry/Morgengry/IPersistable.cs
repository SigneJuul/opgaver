﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Morgengry
{
    interface IPersistable
    {
        void Save();
        void Save(string fileName);
        void Load();
        void Load(string fileName);
    }
}
