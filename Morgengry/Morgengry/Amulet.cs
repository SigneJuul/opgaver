﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Morgengry
{
    public class Amulet : Merchandise, IValuable
    {
        // for notes, see "Books" class
        // public string ItemId { get; set; }
        public string Design { get; set; }
        public Level Quality { get; set; }

        public static double LowQualityValue { get; set; } = 12.5;
        public static double MediumQualityValue { get; set; } = 20.0;
        public static double HighQualityValue { get; set; } = 27.5;







        public Amulet(string itemId, Level quality, string design) 
        {
            base.ItemId = itemId;
            Quality = quality;
            Design = design;
        }

        public Amulet(string itemId, Level quality):
            this (itemId, quality, "")
        {

        }

        public Amulet(string itemId):
            this (itemId, (Level)1, "")
        {

        }

        public double GetValue()
        {
            double returnValue = 0;
            switch (Quality)
            {
                case (Level)0:
                    returnValue = LowQualityValue;
                    break;
                case (Level)1:
                    returnValue = MediumQualityValue;
                    break;
                case (Level)2:
                    returnValue = HighQualityValue;
                    break;
                default:
                    returnValue = 0;
                    break;
            }
            return returnValue;
        }


        public override string ToString()
        {
            return "ItemId: " + ItemId + ", Quality: " + Quality + ", Design: " + Design;
        }
    }
}
