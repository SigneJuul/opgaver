﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilityLib1;

namespace Morgengry
{
    public class MerchandiseRepository
    {
        private List<Merchandise> merchandises = new List<Merchandise>();

        public void AddMerchandise(Merchandise merchandise)
        {
            merchandises.Add(merchandise);
        }

        public Merchandise GetMerchandise(string itemId)
        {
            foreach(Merchandise i in merchandises)
            {
                if (i.ItemId == itemId)
                {
                    return i;
                }
            }
            return null;
        }

        public double GetTotalValue()
        {
            double fullPrice = 0;
            foreach (Merchandise i in merchandises)
            {
                fullPrice += Utility.GetValueOfMerchandise(i); //gets the value from the utility class
            }
            return fullPrice;
        }

    }
}
