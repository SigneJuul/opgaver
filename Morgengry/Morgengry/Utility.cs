﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Morgengry;

namespace UtilityLib1   
{
    public static class Utility
    {

        //public static double GetValueOfBook(Book book)
        //{
        //    return book.Price;
        //}


        //public static double GetValueOfAmulet(Amulet amulet)
        //{
        //    //double valueOfAmuluet = 0;
        //    //switch (amulet.Quality)
        //    //{
        //    //    case (Level)0:
        //    //        valueOfAmuluet = 12.5;
        //    //        break;
        //    //    case (Level)1:
        //    //        valueOfAmuluet = 20.0;
        //    //        break;
        //    //    case (Level)2:
        //    //        valueOfAmuluet = 27.5;
        //    //        break;
        //    //    default:
        //    //        Console.WriteLine("not the right input");
        //    //        break;

        //    //}
        //    //return valueOfAmuluet;
        //} 

        public static double LowQualityValue { get; set; }
        public static double MediumQualityValue { get; set; }
        public static double HighQualityValue { get; set; }
        public static double CourseHourValue { get; set; } 

        static Utility()
        {
            LowQualityValue = 12.5;
            MediumQualityValue = 20.0;
            HighQualityValue = 27.5;
            CourseHourValue = 875.0;
        }


        public static double GetValueOfCourse(Course course)
        {
            int duration = course.DurationInMinutes; //finds the time 
            int hour = 60; // 1 hour = 60
            int pricePrHour = 875;  

            int hourCount = duration / hour; //cause it is int, then it will round down to full number

            if (duration % hour > 0) //if it isnt a full number aka the modulus of duration / 60 
                hourCount++;    // if it is, then the hour count gets added by 1 (cause a new hour is startet)

            return pricePrHour * hourCount;


        }

        public static double GetValueOfMerchandise(Merchandise merchandise)
        {
            double returnValue = 0;

            if (merchandise is Book)
            {
                Book b = merchandise as Book;
                returnValue += b.Price;
            }
            else if(merchandise is Amulet)
            {
                Amulet a = merchandise as Amulet;
                switch (a.Quality)
                {
                    case (Level)0:
                        returnValue = LowQualityValue;
                        break;
                    case (Level)1:
                        returnValue = MediumQualityValue;
                        break;
                    case (Level)2:
                        returnValue = HighQualityValue;
                        break;
                    default:
                        returnValue = 0;
                        break;

                }
               
            }
            return returnValue;
        }
        
    }
}
