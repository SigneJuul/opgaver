﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Morgengry
{
    public class Controller
    {
        /*
        public List<Book> Books { get; set; }
        public List<Amulet> Amulets { get; set; }
        public List<Course> Courses { get; set; }

        public Controller()
        {
            Books = new List<Book>();
            Amulets = new List<Amulet>();
            Courses = new List<Course>();
        }
        */


        //BookRepository br = new BookRepository();
        //AmuletRepository ar = new AmuletRepository();
        // CourseRepository cr = new CourseRepository();
        // MerchandiseRepository mr = new MerchandiseRepository();

       /* public void AddToList(Book book)
        {
            mr.AddMerchandise(book);
        }

        public void AddToList(Amulet amulet)
        {
            mr.AddMerchandise(amulet);
        }

        public void AddToList(Course course)
        {
            cr.AddCourse(course);
        }
       */

        public ValuableRepository ValuableRepo { get; set; }

        public Controller()
        {
            ValuableRepo = new ValuableRepository();
        }

        public void AddToList(IValuable valuable)
        {
            ValuableRepo.AddValuable(valuable);
        }



    }
}
