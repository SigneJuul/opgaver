﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Morgengry
{
    public static class Utility
    {

        public static double GetValueOfBook(Book book)
        {
            return book.Price;
        }

        
        public static double GetValueOfAmulet(Amulet amulet)
        {
            double valueOfAmuluet = 0;
            switch (amulet.Quality)
            {
                case (Level)0:
                    valueOfAmuluet = 12.5;
                    break;
                case (Level)1:
                    valueOfAmuluet = 20.0;
                    break;
                case (Level)2:
                    valueOfAmuluet = 27.5;
                    break;
                default:
                    Console.WriteLine("not the right input");
                    break;

            }
            return valueOfAmuluet;
        } 

        public static double GetValueOfCourse(Course course)
        {
            int duration = course.DurationInMinutes;
            int hour = 60;
            int pricePrHour = 875;

            int hourCount = duration / hour;

            if (duration % hour > 0)
                hourCount++;

            return pricePrHour * hourCount;


        }
        
    }
}
